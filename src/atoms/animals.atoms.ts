import { atom, selector } from 'recoil';
import { ANIMALS } from 'types/animals.types';

export const animalsState = atom({
  key: 'animalsState',
  default: [
    {
      id: 1,
      name: 'Auau',
      type: ANIMALS.DOG,
    },
    {
      id: 2,
      name: 'Miau',
      type: ANIMALS.CAT,
    },
    {
      id: 3,
      name: 'Xium',
      type: ANIMALS.CAT,
    },
    {
      id: 4,
      name: 'Xorom',
      type: ANIMALS.COW,
    },
  ],
});

export const animalFilterState = atom({
  key: 'animalFilterState',
  default: ANIMALS.ALL,
});

export const filteredAnimalsState = selector({
  key: 'animalListState',
  get: ({ get }) => {
    const filter = get(animalFilterState);
    const animals = get(animalsState);
    if (filter === ANIMALS.ALL) return animals;
    return animals.filter((animal) => {
      return animal.type === filter;
    });
  },
});
