import { atom, selector } from 'recoil';

// State control
export const usernameState = atom({
  key: 'username',
  default: '',
});

// Selector
export const upperUsername = selector({
  key: 'upperUsernameState',
  get: ({ get }) => {
    const username = get(usernameState);
    const usernameUpper = username.toUpperCase();
    return usernameUpper;
  },
});
