import React from 'react';
import { useRecoilValue } from 'recoil';
import { filteredAnimalsState } from 'atoms/animals.atoms';
import { ANIMALS } from 'types/animals.types';

export const AnimalsList: React.FC = () => {
  const animals = useRecoilValue(filteredAnimalsState);

  const icon = (type: string) => {
    if (type === ANIMALS.DOG) return '🐶';
    if (type === ANIMALS.CAT) return '🐱';
    if (type === ANIMALS.HORSE) return '🐎';
    if (type === ANIMALS.COW) return '🐄';
    if (type === ANIMALS.SNAKE) return '🐍';
    if (type === ANIMALS.BEAR) return '🐼';
    if (type === ANIMALS.LION) return '🦁';
    if (type === ANIMALS.CETACEAN) return '🐋';
    if (type === ANIMALS.BIRD) return '🐤';
    if (type === ANIMALS.FISH) return '🐟';
    if (type === ANIMALS.CROCODILIA) return '🐊';
    if (type === ANIMALS.INSECT) return '🐛';
    if (type === ANIMALS.RABBIT) return '🐇';
  };

  return (
    <div style={{ marginTop: 16 }}>
      {animals.map((animal) => (
        <div key={animal.id}>
          {animal.name}, {animal.type} {icon(animal.type)}
        </div>
      ))}
    </div>
  );
};
