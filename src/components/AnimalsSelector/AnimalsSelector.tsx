import React from 'react';
import { useSetRecoilState } from 'recoil';
import { animalFilterState } from 'atoms/animals.atoms';
import { ANIMALS } from 'types/animals.types';

const animals = Object.entries(ANIMALS);

export const AnimalsSelector: React.FC = () => {
  const setAnimalFilter = useSetRecoilState(animalFilterState);
  return (
    <div className="buttons">
      <h1>Animals:</h1>
      {animals.map((animal, index) => (
        <button
          key={index}
          onClick={() => setAnimalFilter(animal[1])}
        >
          {animal[1]}
        </button>
      ))}
    </div>
  );
};
