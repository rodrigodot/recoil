import React from 'react';
import {
  useRecoilValue,
  useRecoilState,
  useSetRecoilState,
} from 'recoil';
import { usernameState, upperUsername } from 'atoms/hello.atoms';

export const Hello: React.FC = () => {
  // Only value
  const username = useRecoilValue(usernameState);
  const usernameUpper = useRecoilValue(upperUsername);

  // Only setter
  const updateUsername = useSetRecoilState(usernameState);

  // Value and setter
  const [_username, setUsername] = useRecoilState(usernameState);

  return (
    <>
      <h1>Original: {username}</h1>
      <h1>Maiúsculo: {usernameUpper}</h1>
      <p>Usando useRecoilValue e useSetRecoilState</p>
      Username:
      <input
        placeholder="Digite o username"
        value={username}
        onChange={(e) => updateUsername(e.target.value)}
      />
      <br />
      <p>Usando useRecoilState</p>
      Username:
      <input
        placeholder="Digite o username"
        value={_username}
        onChange={(e) => setUsername(e.target.value)}
      />
    </>
  );
};
