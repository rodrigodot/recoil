import React from 'react';

import { AnimalsSelector, AnimalsList } from 'components';

export const AnimalsPicker: React.FC = () => {
  return (
    <>
      <br />
      <AnimalsSelector />
      <AnimalsList />
    </>
  );
};
