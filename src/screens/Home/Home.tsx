import React from 'react';
import { Hello } from 'components';
import { AnimalsPicker } from 'containers';

export const Home: React.FC = () => {
  return (
    <>
      <Hello />
      <div>
        <AnimalsPicker />
      </div>
    </>
  );
};
